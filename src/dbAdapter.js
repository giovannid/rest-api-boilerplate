const uuid = require("uuid/v1");
const users = [];
const tokens = {};

async function promiseWrapper(callback) {
    return await new Promise(resolve => resolve(callback()));
}

module.exports.findUsers = async () => {
    return promiseWrapper(_ => users);
};

module.exports.findUserById = async id => {
    return promiseWrapper(_ => {
        for (let user in users) {
            if (users[user].id === id) {
                return Object.assign({}, users[user]);
            }
        }
    });
};

module.exports.findUserByEmail = async email => {
    return promiseWrapper(_ => {
        for (let user in users) {
            if (users[user].email === email) {
                return Object.assign({}, users[user]);
            }
        }
        return false;
    });
};

module.exports.addUser = async args => {
    return promiseWrapper(_ => {
        args.id = uuid();
        users.push(args);
        return args.id;
    });
};

module.exports.findTokens = async () => {
    return promiseWrapper(_ => tokens);
};

module.exports.findToken = async token => {
    return promiseWrapper(_ => {
        for (let tokenId of Object.keys(tokens)) {
            if (tokenId === token) {
                return tokens[token];
            }
        }
        return false;
    });
};

module.exports.addToken = async args => {
    return promiseWrapper(_ => {
        tokens[args.token] = args;
        return args;
    });
};
