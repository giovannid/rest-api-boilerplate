const bcrypt = require("bcrypt");
const crypto = require("crypto");
const { fromBase64 } = require("base64url");
const {
    findUsers,
    addUser,
    addToken,
    findUserByEmail
} = require("../dbAdapter");
const compare = bcrypt.compare;
const hash = bcrypt.hash;

async function generateNewToken() {
    return await new Promise((resolve, reject) => {
        crypto.randomBytes(16, (err, data) => {
            err ? reject(err) : resolve(fromBase64(data.toString("base64")));
        });
    });
}

function hashSHA256(string) {
    return crypto
        .createHash("sha256")
        .update(string)
        .digest("base64");
}

async function getUsers(req, res) {
    res.json(await findUsers());
}

async function registerUser(req, res) {
    // get payload from request
    const { name, email, password } = req.body;

    // hash the password
    const hashedPassword = await hash(password, 12);

    // prepare data for database
    let payload = {
        name,
        email,
        password: hashedPassword
    };

    // add user to database
    const id = await addUser(payload);

    // create new token
    const token = await generateNewToken();

    // hash the token for the database
    const hashedToken = hashSHA256(token);

    // add token to database
    await addToken({ token: hashedToken, id });

    // return token and id
    res.json({ token, id });
}

async function loginUser(req, res) {
    // get payload from request
    const { email, password } = req.body;

    // find the user by email
    const user = await findUserByEmail(email);
    // return 403 if you can't find user
    if (!user)
        return res.status(403).send({ error: "Invalid Email and/or Password" });

    // compare the password with the hashed password
    const match = await compare(password, user.password);
    // return 403 if the password doesn't match the one in the database
    if (!match)
        return res.status(403).send({ error: "Invalid Email and/or Password" });

    // create new token
    const token = await generateNewToken();
    // hash the new token
    const hashedToken = hashSHA256(token);

    // add token and id to databse
    const id = user.id;
    await addToken({ token: hashedToken, id });

    // set Auth header and return user payload
    delete user.password;
    res.set("Authorization", token);
    res.json({ token, ...user });
}

async function secretUserPage(req, res) {
    res.json("secret page!");
}

module.exports = {
    registerUser,
    getUsers,
    loginUser,
    secretUserPage,
    hashSHA256
};
