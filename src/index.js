const express = require("express");
const { passport } = require("./passport");
const {
    getUsers,
    registerUser,
    loginUser,
    secretUserPage
} = require("./routes/users");
const { findTokens } = require("./dbAdapter");

const app = express();
const port = 8080;

app.use(express.json());

app.get("/", getUsers);
app.post("/register", registerUser);
app.post("/login", loginUser);
app.get(
    "/secret",
    passport.authenticate("bearer", { session: false }),
    secretUserPage
);
app.get("/tokens", async (req, res) => {
    res.json(await findTokens());
});

app.listen(port, () => console.log(`Listening on port ${port}`));
