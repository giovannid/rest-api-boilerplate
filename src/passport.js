const passport = require("passport");
const Strategy = require("passport-http-bearer").Strategy;
const { findToken } = require("./dbAdapter");
const { hashSHA256 } = require("./routes/users");

// configure bearer strategy for passport
passport.use(
    new Strategy(async function(token, cb) {
        // search db for token
        const dbToken = await findToken(hashSHA256(token));
        // if we can't find it, return false
        if (!dbToken) return cb(null, false);
        // return token and/or user information (role, permissions, etc)
        return cb(null, dbToken);
    })
);

module.exports = {
    passport
};
